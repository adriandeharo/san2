<?php 
session_start();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="./css/style.css" rel="stylesheet" media="screen" />
    <script src="./js/main.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Proyecto BBDD de SAN</title>
</head>

<body>
    <h1>Bienvenido a la BBDD de SAN. </h1>
    <!-- CABECERA  . -->
    <nav class="navbar fixed-top navbar-dark bg-primary navbar-expand-lg navbar-template">
        <a class="navbar-brand" target="_blank" href="https://www.valenciaport.com/"><img src="./img/images/logo-valenciaport-home.svg" /></a>
        <h1>ELIJA UN PUERTO</h1>
        <div class="d-flex flex-row order-2 order-lg-3">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse order-3 order-lg-2" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <p> <?php echo $_SESSION['miusuario'] ?> </p>
                </li>

                <?php
                if ($_SESSION['miusuario'] != "")
                    echo " <li class=\"nav-item\"><a class=\"nav-link\" href=\"./pruebas/log-out.php\" class=\"btn btn-success btn-sm\">
                                <button type=\"button\" class=\"btn btn-warning btn-sm\"> LOGOUT </button> </a></li>";
                else
                    echo " <li class=\"nav-item\"><a class=\"nav-link\" href=\"./pruebas/log-in.html\" class=\"btn btn-success btn-sm\">
                                <button type=\"button\" class=\"btn btn-warning btn-sm\"> LOGIN </button> </a></li>";
                ?>


                <li class="nav-item"><a class="nav-link" href="./plan.html" class="btn btn-success btn-sm">
                        <button type="button" class="btn btn-success btn-sm"> PLAN
                            INSPECCION</button>
                    </a></li>
                <li class="nav-item"><a class="nav-link" href="./inventario.php" class="btn btn-success btn-sm">
                        <button type="button" class="btn btn-success btn-sm"> INVENTARIO
                        </button>
                    </a></li>
            </ul>
        </div>

    </nav>

    <div class="container-fluid" style="margin-top:60px">
        <!-- id="cuerpo" -->
        <!-- CUERPO -->
        <div class="row" style="margin-top:60px">
            <div class="col-sm-12 col-md-4 imgpuertos">
                <a href="./san.php?puerto=gandia">
                    <p>Gandia</p><img src="./img/images/1gandia.jpg" alt="">
                </a>
            </div>
            <div class="col-sm-12 col-md-4 imgpuertos">
                <a href="./san.php?puerto=valencia">
                    <p>Valencia</p><img src="./img/images/1valenciacuadrada.jpg" alt="">
                </a>
            </div>
            <div class="col-sm-12 col-md-4 imgpuertos">
                <a href="./san.php?puerto=sagunto">
                    <p>Sagunto</p><img src="./img/images/3sagunto.jpg" alt="">
                </a>
            </div>
        </div>

        <div class="row" style="margin:80px">
            <div class="col-2"></div>
            <div class="col-sm-12 col-md-4 imgpuertos">
                <a href="./experimental.html">
                    <p style="opacity:0.1">Experimental</p>
                </a>
            </div>

            <div class="col-sm-12 col-md-4 imgpuertos">
                <a href="./san.php?puerto=externo">
                    <p style="opacity:0.1">Externo</p>
                </a>
            </div>
        </div>


        <div class="row" style="margin-left:30%">
            <h4 style="padding:10px">Busqueda por NIF</h4>
            <br>
            <input type="nif" id="buscarnif" />
            <button id="mostrar">Buscar </button>
            <p id="texto"> </p>
        </div>


        <br>
    </div>
    <nav class="navbar fixed-bottom navbar-light bg-primary footer" style="margin-top:30px">
        <div class="col-md-6">
            <p>Autoridad Portuaria de Valencia. Adrian de Haro © 2018 · Todos los derechos reservados</p>
        </div>
    </nav>
</body>

</html> 