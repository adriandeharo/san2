<!DOCTYPE html>
<html lang="es">

<head>
    <title>Proyecto BBDD de SAN</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <!-- <script src="./js/bootstrap.min.js"></script> -->
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <script src="./js/main.js"></script>
    <script src="./js/npm.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<body>
    <?php
    session_start();
    include_once 'funciones.php';
    $fila = $_GET['fila'];
    $columna = $_GET['columna'];  #Lo pasa a mayusculas la primera

    $db = conectaDb();
    ?>

    <!-- CABECERA -->

    <nav class="navbar fixed-top navbar-dark bg-primary navbar-expand-lg navbar-template">
        <a class="navbar-brand" target="_blank" href="https://www.valenciaport.com/"><img src="./img/images/logo-valenciaport-home.svg" /></a>
        <h1>INVENTARIO </h1>
        <div class="d-flex flex-row order-2 order-lg-3">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse order-3 order-lg-2" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <p> <?php if (isset($_SESSION['miusuario']))
                            echo $_SESSION['miusuario'] ?> </p>
                </li>
                <li class="nav-item"><a class="nav-link" href="./inventario.php" class="btn btn-success btn-sm">
                        <button type="button" class="btn btn-success btn-sm"> ATRAS </button>
                    </a></li>
            </ul>
        </div>
    </nav>

    <!-- id="cuerpo" -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12" style="margin:80px">
                <h1> Inventario en Fila <?php echo $fila ?> y Columna <?php echo $columna ?> </h1>
                <table class="table table-hover">
                    <tr class="info-color-dark">
                        <th>Id</th>
                        <th>Item</th>
                        <th>Cantidad</th>
                        <th>Descripcion</th>
                        <th>Modificar</th>
                        <th>Borrar</th>
                    </tr>


                    <?php
                                                                        $sql = "SELECT * FROM inventario  where fila=$fila AND columna=$columna";
                                                                        $result = $db->prepare($sql);
                                                                        $result->execute();
                                                                        if ($result->rowCount() == 0) {
                                                                        } else {
                                                                            foreach ($result as $i) {
                                                                                echo "<tr  class='table-warning'><td> $i[id] </td>  <td> $i[item] </td> <td>$i[cantidad]</td>  <td>$i[descripcion]</td> <td>";
                                                                                echo "Modificar";
                                                                                echo "</td>";
                                                                                echo "<td> <a href=\"./borrarItem.php?id=$i[id]&fila=$i[fila]&columna=$i[columna]\" class=\"btn btn-primary btn-xs";
                                                                                if (isset($_SESSION['miprivilegio']))
                                                                                    if ($_SESSION['miprivilegio'] != "admin")
                                                                                        echo " disabled";
                                                                                echo    " btn-block\"> borrar </a> </td>       </tr>\n";
                                                                            }
                                                                        }

                                                                        ?>

                    <form action="./addItem.php" method="post">
                    <input type="hidden" name="fila" value="<?php echo  $fila; ?>">
                    <input type="hidden" name="columna" value="<?php echo  $columna; ?>">
                        <tr class='table-warning'>
                            <td>#</td>
                            <td><input type="text" name="Item" /></td>
                            <td><input type="text" name="cantidad" /></td>
                            <td><input type="text" name="descripcion" /></td>
                            <td colspan="2"> <input type="submit" value="añadir" class="btn btn-primary btn-xs btn-block" /></td>
                        </tr>
                    </form>


                </table>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-light bg-primary footer" style="margin-top:60px">
        <div>
            <p>Autoridad Portuaria de Valencia. Adrian de Haro © 2018 · Todos los derechos reservados</p>
        </div>
    </nav>

</body>

</html>