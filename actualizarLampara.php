<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="./css/bootstrap.min.css" rel="stylesheet" />  
    <script src="./js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="./css/style.css"/>
    <script src="./js/main.js"></script> 
    <script src="./js/npm.js"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Proyecto BBDD de SAN</title>
</head>

<body>
 
   
    <?php
            $nif=$_GET['nif'];
            $puerto=$_GET['puerto'];
            include_once("funciones.php");
            $usuario="root";
            $contrasena="administrador";
            $db = conectaDb($usuario,$contrasena);
    ?>

    <!-- CABECERA -->
<nav class="navbar fixed-top navbar-dark bg-primary navbar-expand-lg navbar-template">
        <a class="navbar-brand" target="_blank" href="https://www.valenciaport.com/"><img src="./img/images/logo-valenciaport-home.svg" /></a>
        <h1>Caracteristicas del Balizamiento con NIF: <?php echo " ".$nif ?> </h1>
        <div class="d-flex flex-row order-2 order-lg-3">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse order-3 order-lg-2" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="./index.html" class="btn btn-success btn-sm" >CAMBIAR DE PUERTO </a></li>
                <li class="nav-item"><a class="nav-link" href="./san.php" class="btn btn-success btn-sm" >ATRAS </a></li>
            </ul>
        </div>
</nav>  
 
             <!-- CUERPO -->
        <div id="cuerpo">  
          <br>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
      
                <div id="info" class="container">
                        <form action="./modBBDDlampara.php" method="post">
                            <input type="hidden" name="nif" value="<?php echo $nif;?>">
                            <input type="hidden" name="puerto" value="<?php echo $puerto;?>">
                            <table>
                                <tr> <th>Altura</th>     <td><input type="text" name="altura" value="<?php echo $_GET['altura']?>" /></td> </tr>
                                <tr> <th>Elevacion</th> <td><input type="text" name="elevacion" value="<?php echo $_GET['elevacion']?>" /></td> </tr>  
                                <tr> <th>Linterna</th><td><input type="text" name="linterna" value="<?php echo $_GET['linterna'] ?>" /></td></tr>
                                <tr> <th>Alcance</th>     <td><input type="text" name="alcance" value="<?php echo $_GET['alcance'] ?>" /></td> </tr>
                                <tr> <th>Candelas</th>     <td><input type="text" name="candelas" value="<?php echo $_GET['candelas'] ?>" /></td> </tr>
                            </table>
                            <input type="submit" value="Modificar"/>
                        </form>
                </div>
        </div>
</body>
</html>