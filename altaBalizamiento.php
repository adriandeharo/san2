<!DOCTYPE html>
<html>

<head>
    <meta charset="uft-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/main.js" type='text/javascript'></script>
    <title>Proyecto BBDD de SAN</title>
</head>

<body>
    <?php
    include 'funciones.php';
    $db = conectaDb();
    ?>

    <!-- CABECERA -->
    <nav class="navbar fixed-top navbar-dark bg-primary navbar-expand-lg navbar-template">
        <a class="navbar-brand" target="_blank" href="https://www.valenciaport.com/"><img src="./img/images/logo-valenciaport-home.svg" /></a>
        <h1> INSERTE UNA NUEVA AYUDA A LA NAVEGACION</h1>
        <div class="d-flex flex-row order-2 order-lg-3">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse order-3 order-lg-2" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="./index.html" class="btn btn-success btn-sm">
                        <button type="button" class="btn btn-success btn-sm"> INICIO </button>

                    </a></li>
                <li class="nav-item"><a class="nav-link" href="./san.php?puerto=<?php echo $puerto ?>" class="btn btn-success btn-sm">
                        <button type="button" class="btn btn-success btn-sm"> ATRAS</button>
                    </a></li>
            </ul>
        </div>
    </nav>



    <div class="container">
        <div class="row" style="margin-top: 100px;">
            <div class="col-lg-12">
                <form action="./altaNueva.php" method="post" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <th>NIF</th>
                            <td><input type="text" name="nif" /></td>
                        </tr>
                        <tr>
                            <th>Num.Internacional</th>
                            <td><input type="text" name="num_internacional" /></td>
                        </tr>
                        <tr>
                            <th>Tipo</th>
                            <td><input type="text" name="tipo" /></td>
                        </tr>
                        <tr>
                            <th>Alcance</th>
                            <td><input type="text" name="alcance" /></td>
                        </tr>
                        <tr>
                            <th>Apariencia</th>
                            <td><input type="text" name="apariencia" /></td>
                        </tr>
                        <tr>
                            <th>Periodo</th>
                            <td><input type="text" name="periodo" /></td>
                        </tr>
                        <tr>
                            <th>Caracteristica</th>
                            <td><input type="text" name="caracteristica" /></td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <th>Puerto</th>
                            <td><input type="text" name="puerto" /></td>
                        </tr>
                        <tr>
                            <th>Numero Local</th>
                            <td><input type="text" name="num_local" /></td>
                        </tr>
                        <tr>
                            <th>Localizacion</th>
                            <td><input type="text" name="localizacion" /></td>
                        </tr>
                        <tr>
                            <th>Latitud</th>
                            <td><input type="text" name="latitud" /></td>
                        </tr>
                        <tr>
                            <th>Longitud</th>
                            <td><input type="text" name="longitud" /></td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <th>Altura</th>
                            <td><input type="text" name="altura" /></td>
                        </tr>
                        <tr>
                            <th>Elevacion</th>
                            <td><input type="text" name="elevacion" /></td>
                        </tr>
                        <tr>
                            <th>Alcance</th>
                            <td><input type="text" name="alcance" /></td>
                        </tr>
                        <tr>
                            <th>Linterna</th>
                            <td><input type="text" name="linterna" /></td>
                        </tr>
                        <tr>
                            <th>Cnadelas</th>
                            <td><input type="text" name="candelas" /></td>
                        </tr>
                    </table>
                    <br>
                    <table>
                        <tr>
                            <th>Insertar foto...</th>
                            <td><input name="foto" type="file" /></td>
                        </tr>
                    </table>
                    <input type="submit" value="ENVIAR ALTA" class="btn btn-success btn-sm" />
                </form>
            </div>
        </div>
    </div>
</body>

</html> 